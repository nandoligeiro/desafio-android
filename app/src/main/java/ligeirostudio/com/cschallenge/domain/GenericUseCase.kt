package ligeirostudio.com.cschallenge.domain

import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.Subscriptions

abstract class GenericUseCase <T> : Interactor<T> {


    private var subscription = Subscriptions.empty()

    abstract fun createObservable(): Observable<T>

    override fun execute(subscriber: Subscriber<T>) {

        subscription = createObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber)
    }

    override fun execute(): Observable<T> {
        return createObservable()
    }

    fun unSubscribe() {

        if (!subscription.isUnsubscribed) {
            subscription.unsubscribe()
        }

    }

}
