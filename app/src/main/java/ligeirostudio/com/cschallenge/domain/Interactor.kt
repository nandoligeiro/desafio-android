package ligeirostudio.com.cschallenge.domain

import rx.Observable
import rx.Subscriber


interface Interactor<T> {

    fun execute (subscriber : Subscriber<T>)
    fun execute() : Observable<T>
}