package ligeirostudio.com.cschallenge.data.api

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

interface GithubApiDef {

    @GET("/search/repositories")
    fun getJavaRepositories(@Query("q") q: String,
                                     @Query("sort") sort: String,
                                     @Query("page") page: String): Observable<SearchRepos>


    @GET("repos/{owner}/{repo}/pulls")
    fun getPulls(@Path("owner") owner: String, @Path("repo") repo: String): Observable<List<Pull>>


    @GET("users/{user}")
    abstract fun getUser(@Path("user") user: String): Observable<Profile>
}
