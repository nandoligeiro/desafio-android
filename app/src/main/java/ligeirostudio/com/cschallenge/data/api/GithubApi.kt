package ligeirostudio.com.cschallenge.data.api

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable

class GithubApi {

    val api: GithubApiDef

    init {

        val loggin = HttpLoggingInterceptor()
        loggin.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.addInterceptor(loggin)

        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)

        val gson = gsonBuilder.create()


        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient.build())
                .build()
        api = retrofit.create<GithubApiDef>(GithubApiDef::class.java)
    }


    fun loadRepos(page: String): Observable<SearchRepos> {

        return api.getJavaRepositories("language:Java", "stars", page)

    }

    fun loadPullRequests(owner: String, repo: String): Observable<List<Pull>> {

        return api.getPulls(owner, repo)

    }

    fun getUser(user: String): Observable<Profile> {

        return api.getUser(user)

    }
}
