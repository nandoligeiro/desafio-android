package ligeirostudio.com.cschallenge.data.api


data class Profile(
		val login: String, //akarnokd
		val id: Int, //1269832
		val avatar_url: String, //https://avatars2.githubusercontent.com/u/1269832?v=4
		val gravatar_id: String,
		val url: String, //https://api.github.com/users/akarnokd
		val html_url: String, //https://github.com/akarnokd
		val followers_url: String, //https://api.github.com/users/akarnokd/followers
		val following_url: String, //https://api.github.com/users/akarnokd/following{/other_user}
		val gists_url: String, //https://api.github.com/users/akarnokd/gists{/gist_id}
		val starred_url: String, //https://api.github.com/users/akarnokd/starred{/owner}{/repo}
		val subscriptions_url: String, //https://api.github.com/users/akarnokd/subscriptions
		val organizations_url: String, //https://api.github.com/users/akarnokd/orgs
		val repos_url: String, //https://api.github.com/users/akarnokd/repos
		val events_url: String, //https://api.github.com/users/akarnokd/events{/privacy}
		val received_events_url: String, //https://api.github.com/users/akarnokd/received_events
		val type: String, //User
		val site_admin: Boolean, //false
		val name: String, //David Karnok
		val company: String, //MTA SZTAKI
		val blog: String, //https://akarnokd.blogspot.com
		val location: String, //Budapest, Hungary
		val email: Any, //null
		val hireable: Any, //null
		val bio: Any, //null
		val public_repos: Int, //81
		val public_gists: Int, //137
		val followers: Int, //648
		val following: Int, //0
		val created_at: String, //2011-12-17T16:32:55Z
		val updated_at: String //2017-10-31T17:25:22Z
)