package ligeirostudio.com.cschallenge.presentation.pull

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ligeirostudio.com.cschallenge.R
import ligeirostudio.com.cschallenge.data.api.Pull
import ligeirostudio.com.cschallenge.databinding.CardPullsBinding
import ligeirostudio.com.cschallenge.presentation.webview.WebViewActivity

class PullsAdapter(private val context: Context, private val pulls: List<Pull>) : RecyclerView.Adapter<PullsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val mBinding: CardPullsBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.card_pulls, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        if (pulls.isNotEmpty()) {
            val pull = pulls[position]
            holder?.getBinding()?.pull = pull

            holder?.itemView?.setOnClickListener {

                val intent = Intent(context, WebViewActivity::class.java)
                intent.putExtra("PULL_REPO", pull.base.repo.name)
                intent.putExtra("PULL_OWNER", pull.base.repo.owner.login)
                intent.putExtra("PULL_NUMBER", pull.number)

                context.startActivity(intent)

            }

        }
    }

    override fun getItemCount(): Int {
        return pulls.size
    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        private val binding: CardPullsBinding = DataBindingUtil.bind(itemView)

        fun getBinding(): CardPullsBinding {
            return binding
        }
    }

}