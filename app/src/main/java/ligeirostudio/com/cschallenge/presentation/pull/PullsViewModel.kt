package ligeirostudio.com.cschallenge.presentation.pull

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import ligeirostudio.com.cschallenge.data.api.Pull
import ligeirostudio.com.cschallenge.domain.PullRequestUseCase

class PullsViewModel: ViewModel(){


    private val subscriber = PullsSubscriber(this)

    fun loadPullRequests(owner: String, repo: String) {
        val prCase = PullRequestUseCase(owner, repo)
        prCase.execute(subscriber)
    }

    var pulls = MutableLiveData<List<Pull>>().apply { value = null }

    fun updateItems(pull: List<Pull>) {
        pulls.value = pull
    }


}