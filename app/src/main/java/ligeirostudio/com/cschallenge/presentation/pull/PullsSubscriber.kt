package ligeirostudio.com.cschallenge.presentation.pull

import ligeirostudio.com.cschallenge.data.api.Pull
import rx.Subscriber

class PullsSubscriber(pullsViewModel: PullsViewModel) : Subscriber<List<Pull>>() {

    private var viewModel = pullsViewModel

    override fun onCompleted() {
    }

    override fun onError(e: Throwable?) {
    }

    override fun onNext(t: List<Pull>?) {
        if (t != null) {
            viewModel.updateItems(t)
        }
    }


}
