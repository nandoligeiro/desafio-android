package ligeirostudio.com.cschallenge.presentation.webview

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.webview_pull.*
import ligeirostudio.com.cschallenge.R
import java.io.Serializable


@SuppressLint("Registered")
class WebViewActivity : AppCompatActivity() {


    private val webViewModel: WebViewViewModel by lazy {
        ViewModelProviders.of(this).get(WebViewViewModel::class.java)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview_pull)

        val repo: String = intent.getStringExtra("PULL_REPO")
        val owner: String = intent.getStringExtra("PULL_OWNER")
        val number: Serializable = intent.getSerializableExtra("PULL_NUMBER")

        val url: String = getString(R.string.github) + owner + "/" + repo + "/" + "pull" + "/" + number.toString()
        webViewModel.loadWebView(webview, url)


    }


}
