package ligeirostudio.com.cschallenge.presentation.home

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ligeirostudio.com.cschallenge.R
import ligeirostudio.com.cschallenge.data.api.Item
import ligeirostudio.com.cschallenge.databinding.CardHomeBinding
import ligeirostudio.com.cschallenge.presentation.pull.PullsActivity


class HomeAdapter(private val context: Context) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    private var itemList = mutableListOf<Item>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {

        val mBinding: CardHomeBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.card_home, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        if (itemList.isNotEmpty()) {
            val item = itemList[position]
            holder?.getBinding()?.item = item

            holder?.itemView?.setOnClickListener {

                val intent = Intent(context, PullsActivity::class.java)
                intent.putExtra("ITEM_OWNER", item.owner.login)
                intent.putExtra("ITEM_REPO", item.name)
                context.startActivity(intent)
            }
        }

    }

    override fun getItemCount(): Int {
        return itemList.size
    }


    fun addItems(searchRepos: List<Item>) {
        itemList = searchRepos as MutableList<Item>
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        private val binding: CardHomeBinding = DataBindingUtil.bind(itemView)

        fun getBinding(): CardHomeBinding {
            return binding
        }
    }

}
